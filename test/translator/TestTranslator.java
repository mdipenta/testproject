package translator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestTranslator {

	@Test
	public void testGetSentence() {
		String sentence="hello world";
		Translator t=new Translator(sentence);
		assertEquals(sentence,t.getSentence());
		
	}
	
	@Test
	public void testTranslateEmptySentence() {
		String sentence="";
		Translator t=new Translator(sentence);
		assertEquals("nil",t.translate());
		
	}
	
	@Test
	public void testTranslateStartsVowelEndsY() {
		String sentence="array";
		Translator t=new Translator(sentence);
		assertEquals(sentence+"nay",t.translate());
		
	}
	

}
